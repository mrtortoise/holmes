﻿namespace Logic
{
    public enum Direction
    {
        Clockwise,
        AntiClockwise
    }
}
﻿namespace Logic
{
    public interface IActor
    {
        Direction Direction { get; }
        int IntervelTicks { get; }
        string Name { get; }
        IRoom Room { get; }

        void MoveRoom();

        bool HasRoomCollision { get; }
    }
}
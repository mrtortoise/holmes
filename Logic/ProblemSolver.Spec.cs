﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Xunit;

namespace Logic
{
    public class ProblemSolver
    {
        [Fact]
        public void SolverTracksTicksProperly()
        {
            var agent = new Mock<IActor>();
            var emitter = new TickEmitter(5);
            agent.Setup(i => i.IntervelTicks).Returns(5);

            var solver = new Solver(emitter, new List<IActor>() {agent.Object});

            Assert.Equal(0, solver.CurrentTicks);
            emitter.Tick();
            Assert.Equal(5, solver.CurrentTicks);
            emitter.Tick();
            Assert.Equal(10, solver.CurrentTicks);
            emitter.Tick();
            Assert.Equal(15, solver.CurrentTicks);

        }

        [Fact]
        public void CreateSolverWithAgentVerifyMovesOnCorrectTick()
        {
            var agent = new Mock<IActor>();
            bool calledMove = false;

            agent.Setup(i => i.MoveRoom()).Callback(() => calledMove = true);
            agent.Setup(i => i.IntervelTicks).Returns(15);
            var emitter = new TickEmitter(5);
            var solver = new Solver(emitter, new List<IActor>() {agent.Object});
            
            //0
            Assert.False(calledMove);
            emitter.Tick();
            //5
            Assert.False(calledMove);
            emitter.Tick();
            //10
            Assert.False(calledMove);
            emitter.Tick();
            //15
            Assert.True(calledMove);
            emitter.Tick();
        }

        [Fact]
        public void TestForWinStateFail()
        {
            var emitter = new TickEmitter(5);
            var room1 = new Mock<IRoom>();
            var room2 = new Mock<IRoom>();

            var agent = new Mock<IActor>();
            agent.Setup(i => i.Room).Returns(room1.Object);
            agent.Setup(i => i.IntervelTicks).Returns(100);

            var agent2 = new Mock<IActor>();
            agent2.Setup(i => i.Room).Returns(room2.Object);
            agent2.Setup(i => i.IntervelTicks).Returns(100);

            var solver = new Solver(emitter, new List<IActor>() {agent2.Object, agent.Object});

            var winzor = false;
            solver.OnWin += (sender, args) => { winzor = true; };

            emitter.Tick();
            Assert.Equal(false, winzor);
        }

        [Fact]
        public void OneActorAutoWin()
        {
            var emitter = new TickEmitter(5);
            var rooms = RoomFactory.GenerateRooms(new string[]{"test"});
            var actor = new Actor("test", rooms.Values.First(), Direction.AntiClockwise, 15);

            var solver = new Solver(emitter, new List<IActor>() {actor});
            bool winner = false;
            solver.OnWin += (sender, s) => { winner = true; };

            emitter.Tick();
            Assert.True(winner);
        }
        

        [Fact]
        public void TestForWinStateSuccess()
        {
            var emitter = new TickEmitter(5);
            var room1 = new Mock<IRoom>();
            var room2 = new Mock<IRoom>();
            var room3 = new Mock<IRoom>();

            var agent = new Mock<IActor>();
            agent.Setup(i => i.Room).Returns(room1.Object);
            agent.Setup(i => i.IntervelTicks).Returns(15);

            agent.Setup(i => i.MoveRoom()).Callback(()=>agent.Setup(i => i.Room).Returns(room3.Object));

            var agent2 = new Mock<IActor>();
            agent2.Setup(i => i.Room).Returns(room2.Object);
            agent2.Setup(i => i.IntervelTicks).Returns(15);

            agent2.Setup(i => i.MoveRoom()).Callback(() => agent2.Setup(i => i.Room).Returns(room3.Object));

            var solver = new Solver(emitter, new List<IActor>() { agent2.Object, agent.Object });

            var winzor = false;
            solver.OnWin += (sender, args) => { winzor = true; };

            emitter.Tick();
            Assert.Equal(false, winzor);
            emitter.Tick();
            Assert.Equal(false, winzor);
            emitter.Tick();
            Assert.Equal(true, winzor);
        }

        [Fact]
        public void IfRoomOCcupiedAgentMovesToNextRoomUnlessWin()
        {
            var emitter = new TickEmitter(5);
            var room1 = new Mock<IRoom>();
            var room2 = new Mock<IRoom>();
            var room3 = new Mock<IRoom>();

            room1.Setup(i => i.Name).Returns("r1");
            room2.Setup(i => i.Name).Returns("r2");
            room3.Setup(i => i.Name).Returns("r3");

            var agent = new Mock<IActor>();
           
            agent.Setup(i => i.IntervelTicks).Returns(5);
            agent.Setup(i => i.HasRoomCollision).Returns(true);
            agent.Setup(i => i.Name).Returns("one");

            //each time move is called we want to move to next item in this list
            var rooms = new List<IRoom>() { room1.Object, room2.Object, room3.Object};
            int callNumber = 0;
            agent.Setup(i => i.Room).Returns(rooms[callNumber]);

            agent.Setup(i => i.MoveRoom()).Callback(() =>
            {
                callNumber++;
                agent.Setup(i => i.Room).Returns(rooms[callNumber]);
            });

            var agent2 = new Mock<IActor>();
            agent2.Setup(i => i.Room).Returns(room2.Object);
            agent2.Setup(i => i.IntervelTicks).Returns(999);
            agent2.Setup(i => i.HasRoomCollision).Returns(true);
            agent2.Setup(i => i.Name).Returns("two");

            var solver = new Solver(emitter, new List<IActor>() { agent2.Object, agent.Object });

            var winzor = false;
            solver.OnWin += (sender, args) => { winzor = true; };

            emitter.Tick();
            Assert.Equal(true, winzor);
            Assert.Equal(room2.Object, agent.Object.Room);
        }

        [Fact]
        public void IfRoomOccupiedAndCollidableAgentMovesToNextRoom()
        {
            var emitter = new TickEmitter(5);
            var room1 = new Mock<IRoom>();
            var room2 = new Mock<IRoom>();
            var room3 = new Mock<IRoom>();

            room1.Setup(i => i.Name).Returns("r1");
            room2.Setup(i => i.Name).Returns("r2");
            room3.Setup(i => i.Name).Returns("r3");

            var agent = new Mock<IActor>();

            agent.Setup(i => i.IntervelTicks).Returns(5);
            agent.Setup(i => i.HasRoomCollision).Returns(true);
            agent.Setup(i => i.Name).Returns("one");

            //each time move is called we want to move to next item in this list
            var rooms = new List<IRoom>() { room1.Object, room2.Object, room3.Object };
            int callNumber = 0;
            agent.Setup(i => i.Room).Returns(rooms[callNumber]);

            agent.Setup(i => i.MoveRoom()).Callback(() =>
            {
                callNumber++;
                agent.Setup(i => i.Room).Returns(rooms[callNumber]);
            });

            var agent2 = new Mock<IActor>();
            agent2.Setup(i => i.Room).Returns(room2.Object);
            agent2.Setup(i => i.IntervelTicks).Returns(999);
            agent2.Setup(i => i.HasRoomCollision).Returns(true);
            agent2.Setup(i => i.Name).Returns("two");

            var agent3 = new Mock<IActor>();
            agent3.Setup(i => i.Room).Returns(room3.Object);
            agent3.Setup(i => i.IntervelTicks).Returns(999);
            agent3.Setup(i => i.HasRoomCollision).Returns(false); // allows actor to land on this room
            agent3.Setup(i => i.Name).Returns("three");

            var solver = new Solver(emitter, new List<IActor>() { agent2.Object, agent.Object,agent3.Object });

            var winzor = false;
            solver.OnWin += (sender, args) => { winzor = true; };

            emitter.Tick();
            Assert.Equal(false, winzor);
            Assert.Equal(room3.Object, agent.Object.Room);
        }
    }
}

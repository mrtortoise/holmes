﻿using System;
using Moq;
using Xunit;

namespace Logic
{
    public class ActorSpec
    {
        [Fact]
        public void ActorHasCorrectNameStartsInARoomHasDirectionAndInterviewLengthAndCollision()
        {
            var room = new Mock<IRoom>();
            var ut = new Actor("Holmes", room.Object, Direction.Clockwise, 15, true);
            Assert.Equal("Holmes",ut.Name);
            Assert.Equal(room.Object, ut.Room);
            Assert.Equal(Direction.Clockwise,ut.Direction);
            Assert.Equal(15,ut.IntervelTicks);
        }

        [Fact]
        public void ActorCanChangeRoomClockWise()
        {
            var room = new Mock<IRoom>();
            var room2 = new Mock<IRoom>();

            var ut = new Actor("Holmes", room.Object, Direction.Clockwise, 15);

            room.Setup(i => i.ClockWiseRoom).Returns(room2.Object);

            ut.MoveRoom();
            Assert.Equal(room2.Object, ut.Room);
        }

        [Fact]
        public void ActorCanChangeRoomAntiClockWise()
        {
            var room = new Mock<IRoom>();
            var room2 = new Mock<IRoom>();

            var ut = new Actor("Holmes", room.Object, Direction.AntiClockwise, 15);

            room.Setup(i => i.AntiClockwiseRoom).Returns(room2.Object);

            ut.MoveRoom();
            Assert.Equal(room2.Object, ut.Room);
        }

        [Fact]
        public void ActorHasRoomCollisionByDefault()
        {
            var room = new Mock<IRoom>();
            var ut = new Actor("Holmes", room.Object, Direction.AntiClockwise, 15);

            Assert.True(ut.HasRoomCollision);
        }
    }
}
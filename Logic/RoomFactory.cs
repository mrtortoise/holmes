﻿using System.Collections.Generic;
using System.Linq;

namespace Logic
{
    /// <summary>
    /// Constructs and links rooms according to order in input
    /// </summary>
    public class RoomFactory
    {
        /// <summary>
        /// Generates a essentially linked list in a ring buffer with either end connected
        /// </summary>
        /// <param name="strings"></param>
        /// <returns>THe list</returns>
        public static Dictionary<string,IRoom> GenerateRooms(string[] strings)
        {
            var rooms = strings.Select(i => new Room(i)).ToList();

            for (int i = 0; i < strings.Length; i++)
            {
                int anticlockwise = i-1;
                int clockwise = i + 1;

                if (anticlockwise < 0)
                {
                    anticlockwise = strings.Length - 1;
                }

                if (clockwise >= strings.Length)
                {
                    clockwise = 0;
                }

                rooms[i].Hookup(rooms[anticlockwise], rooms[clockwise]);
            }

            return rooms.Select(i => (IRoom) i).ToDictionary(i => i.Name);
        }
    }
}
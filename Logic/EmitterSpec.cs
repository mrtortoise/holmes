using Xunit;

namespace Logic
{
    public class EmitterSpec
    {
        [Fact]
        public void TickEmitterEmitsCorrectTimeDuration()
        {
            var tickSize = 5;
            var emitter = new TickEmitter(tickSize);
            bool eventFired = false;

            emitter.OnTick += (sender, i) =>
            {
                eventFired = true;
                Assert.Equal(tickSize, i);
            };
            emitter.Tick();
            Assert.True(eventFired);
        }
    }
}
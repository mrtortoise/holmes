using System;
using System.Collections.Generic;
using System.Linq;

namespace Logic
{
    public class Solver
    {
        public List<IActor> Actors { get; set; }
        public int CurrentTicks { get; private set; }
        public event EventHandler<string> OnWin;

        public Solver(TickEmitter emitter, List<IActor> actors)
        {
            if (actors == null) throw new ArgumentNullException(nameof(actors));
            if (actors.Count == 0) throw new ArgumentException("Value cannot be an empty collection.", nameof(actors));

            CurrentTicks = 0;
            Actors = actors;
            emitter.OnTick += Emitter_OnTick;
        }

        private void Emitter_OnTick(object sender, int ticks)
        {
            CurrentTicks += ticks;
            if (Actors.Count == 1)
            {
                RaiseWin();
            }

            foreach (var actor in Actors)
            {
                int result = 0;
                Math.DivRem(CurrentTicks, actor.IntervelTicks, out result);

                if (result == 0)
                {
                    if (MoveRoom(actor)) return;
                }
            }
        }

        private bool MoveRoom(IActor actor)
        {
            actor.MoveRoom();

            IRoom room = actor.Room;
            if (Actors.TrueForAll(i => i.Room == room))
            {
                RaiseWin();
                return true;
            }
            if (Actors.Any(i => i.HasRoomCollision && i.Room == actor.Room && i.Name != actor.Name))
            {
                return MoveRoom(actor);
            }

            return false;
        }

        private void RaiseWin()
        {
            if (OnWin != null)
            {
                OnWin(this, Actors[0].Room.Name + "@" + CurrentTicks);
            }
        }
    }
}
﻿namespace Logic
{
    /// <summary>
    /// represents a room in the problem
    /// </summary>
    public class Room : IRoom
    {
        public IRoom AntiClockwiseRoom => _antiClockwiseRoom;

        public IRoom ClockWiseRoom => _clockWiseRoom;

        private IRoom _antiClockwiseRoom;
        private IRoom _clockWiseRoom;

        public string Name { get; }

        public Room(string name)
        {
            Name = name;
        }

        public Room(string name, IRoom antiClockwiseRoom, IRoom clockWiseRoom) : this(name)
        {
            _antiClockwiseRoom = antiClockwiseRoom;
            _clockWiseRoom = clockWiseRoom;
        }

        public void Hookup(IRoom antiClockWise, IRoom clockwise)
        {
            _antiClockwiseRoom = antiClockWise;
            _clockWiseRoom = clockwise;
        }
    }
}
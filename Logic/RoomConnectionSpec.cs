﻿using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Logic
{
    public class RoomConnectionSpec
    {
        [Fact]
        public void WhenCreateARoomItHasAName()
        {
            var testName = "test";
            var room = new Room(testName);
            Assert.Equal(testName, room.Name);
        }

        [Fact]
        public void CreateThreeRoomsConnectOneClockWise()
        {
            string r1Name = Rooms.Mustard;
            string r2Name = Rooms.Green;
            string r3Name = Rooms.Plum;

            var room1 = new Room(r1Name);
            var room3 = new Room(r3Name);
            var room2 = new Room(r2Name, room1, room3);

            Assert.Equal(room3, room2.ClockWiseRoom);
            Assert.Equal(room1, room2.AntiClockwiseRoom);
        }

        [Fact]
        public void CreateOneRoomInALoop()
        {
            
            List<IRoom> rooms = RoomFactory.GenerateRooms(new string[] {Rooms.Mustard}).Values.ToList();

            Assert.Equal(1, rooms.Count);
            Assert.Equal(rooms[0],rooms[0].AntiClockwiseRoom);
            Assert.Equal(rooms[0], rooms[0].ClockWiseRoom);
        }

        [Fact]
        public void Test2Rooms()
        {
            var rooms = RoomFactory.GenerateRooms(new[] {"lhs", "rhs"});

            Assert.Equal(rooms["lhs"],rooms["rhs"].ClockWiseRoom);
            Assert.Equal(rooms["lhs"], rooms["rhs"].AntiClockwiseRoom);
        }

        [Fact]
        public void TestAllRoomsWillWork()
        {
            List<IRoom> rooms = RoomFactory.GenerateRooms(new string[] {Rooms.Mustard, Rooms.Green, Rooms.Plum, Rooms.Peacock, Rooms.Scarlett, Rooms.White}).Values.ToList();

            var roomDic = rooms.ToDictionary(room => room.Name);

            Assert.Equal(roomDic[Rooms.Green], roomDic[Rooms.Mustard].ClockWiseRoom);

            Assert.Equal(roomDic[Rooms.White], roomDic[Rooms.Mustard].AntiClockwiseRoom);
        }
    }
}
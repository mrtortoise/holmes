﻿namespace Logic
{
    public interface IRoom
    {
        IRoom AntiClockwiseRoom { get; }
        IRoom ClockWiseRoom { get; }
        string Name { get; }
    }
}
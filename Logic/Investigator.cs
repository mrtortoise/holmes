﻿namespace Logic
{
    public class Actor : IActor
    {
        private IRoom _room;
        public string Name { get; }

        public IRoom Room
        {
            get { return _room; }
        }

        public Direction Direction { get; }
        public int IntervelTicks { get; }

        public bool HasRoomCollision { get; }

        public Actor(string name, IRoom startingRoom, Direction direction, int intervelTicks,bool hasCollision = true)
        {
            Name = name;
            _room = startingRoom;
            Direction = direction;
            IntervelTicks = intervelTicks;
            HasRoomCollision = hasCollision;
        }

        public void MoveRoom()
        {
            if (Direction == Direction.AntiClockwise)
            {
                _room = _room.AntiClockwiseRoom;
            }
            else
            {
                _room = _room.ClockWiseRoom;
            }
        }

       
    }
}
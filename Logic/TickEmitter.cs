using System;

namespace Logic
{
    public class TickEmitter
    {
        public int TickSize { get; set; }
        public event EventHandler<int> OnTick;

        public TickEmitter(int tickSize)
        {
            TickSize = tickSize;
        }

        public void Tick()
        {
            if (OnTick != null)
            {
                OnTick(this, TickSize);
            }
        }
    }
}
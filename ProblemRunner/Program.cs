﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;

namespace ProblemRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            var emitter = new TickEmitter(5);
            Dictionary<string, IRoom> rooms = RoomFactory.GenerateRooms(new string[] { Rooms.Mustard, Rooms.Green, Rooms.Plum, Rooms.Peacock, Rooms.Scarlett, Rooms.White });

            var holmes = new Actor("holmes", rooms[Rooms.Mustard], Direction.Clockwise, 15);
            var watson = new Actor("watson", rooms[Rooms.White], Direction.AntiClockwise, 20);
            var wellington = new Actor("wellington", rooms[Rooms.Mustard], Direction.AntiClockwise, 30,false);

            var solver = new Solver(emitter, new List<IActor>() {holmes, watson, wellington});

            bool ended = false;
            solver.OnWin += (s, a) =>
            {
                Console.WriteLine(a);
                ended = true;
            };

            while (!ended)
            {
                emitter.Tick();
            }

            Console.ReadLine();
        }
    }
}
